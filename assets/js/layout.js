

// fix cứng cho thanh menu ở đầu trang
window.onscroll = function () { myFunction() };
var mainMenu = document.getElementById("main-menu");
var sticky = mainMenu.offsetTop;
var iconScrollTop = document.getElementById("back-to-top");
iconScrollTop.style.display = "none";


function myFunction() {
    if (window.pageYOffset >= sticky) {
        if(!mainMenu.classList.contains("sticky")) {
            mainMenu.classList.add("sticky")
        }
    } else {
        mainMenu.classList.remove("sticky");
    }


    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        iconScrollTop.classList.add("show-scroll-to-top")

    } else {
        iconScrollTop.classList.remove("show-scroll-to-top")

    }

}


// đóng menu mobile khi màn to
window.onresize = reportWindowSize;
function reportWindowSize() {
    let width = window.innerWidth;
    if (width > 1024) document.querySelector(".board-menu-mobile").classList.remove("clicked");
}



// bắt sự kiện toggle icon mobile
var iconTogle = document.querySelector('.navbar-toggler')
var menuMobile = document.querySelector('.board-menu-mobile')
iconTogle.onclick = function () { onClickTogleMenu() };

function onClickTogleMenu() {
    menuMobile.classList.toggle("clicked")
}




// When the user clicks on the button, scroll to the top of the document
iconScrollTop.onclick = function () { topFunction() };
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

